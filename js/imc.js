function calcularIMC() {
    const peso = parseFloat(document.getElementById("peso").value);
    const altura = parseFloat(document.getElementById("altura").value);

    if (isNaN(peso) || isNaN(altura) || peso <= 0 || altura <= 0) {
        alert("Por favor, ingrese valores válidos para peso y altura.");
        return;
    }

    const imc = peso / (altura * altura);
    const resultado = document.getElementById("resultado");
    resultado.textContent = `Su IMC es: ${imc.toFixed(2)}`;

    let nivel = "";
    let imagen = "";
    let calorias = "";

    if (imc < 18.5) {
        nivel = "Peso por debajo de lo normal";
        imagen = "/img/1.jpeg";
        calorias = "Calorías recomendadas: 2000-2200 kcal/día";
    } else if (imc < 25) {
        nivel = "Peso saludable";
        imagen = "/img/2.jpeg";
        calorias = "Calorías recomendadas: 1800-2000 kcal/día";
    } else if (imc < 30) {
        nivel = "Sobrepeso";
        imagen = "/img/3.jpeg";
        calorias = "Calorías recomendadas: 1600-1800 kcal/día";
    } else if (imc < 35) {
        nivel = "Obesidad Tipo I";
        imagen = "/img/4.jpeg";
        calorias = "Calorías recomendadas: 1400-1600 kcal/día";
    } else if (imc < 40) {
        nivel = "Obesidad Tipo II";
        imagen = "/img/5.jpeg";
        calorias = "Calorías recomendadas: 1200-1400 kcal/día";
    } else {
        nivel = "Obesidad Tipo III";
        imagen = "/img/6.jpeg";
        calorias = "Calorías recomendadas: < 1200 kcal/día";
    }

    resultado.textContent += ` (${nivel})`;
    
    const caloriasRecomendadas = document.getElementById("calorias-recomendadas");
    caloriasRecomendadas.textContent = calorias;

    // Mostrar la imagen correspondiente
    const imagenElement = document.getElementById("imagen");
    imagenElement.src = imagen;
    imagenElement.alt = nivel;
}
