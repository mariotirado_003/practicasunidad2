const tablaAlum = document.getElementById('tabla-alumnos');
let alumno = [{

    "matricula":"2021030117",
    "nombre":"Tirado Rios Luis Mario",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/mario.jpeg">'
},{
    "matricula":"2021031015",
    "nombre":"Flores Perez Jesus Adolfo",  
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/adolfo.jpeg">'
},{
    "matricula":"2021030815",
    "nombre":"Arias Tirado Mateo",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/mateo.jpeg">'
},{ 
    "matricula":"2021030314",
    "nombre":"Qui Mora Angel Ernesto",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/qui.jpeg">'
},{
    "matricula":"2021030143",
    "nombre":"Tirado Rios Oscar de Jesus",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/bro.jpg">'
},{
    "matricula":"2020030321",
    "nombre":"Ontiveros Govea Yair Alejandro",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/govea.jpeg">'
},{
    "matricula":"2021030652",
    "nombre":"Quezada Lara Jesus Alejandro",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/lara.jpeg">'
},{
    "matricula":"2021030142",
    "nombre":"Aguilar Romero Jonathan Jesus",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/jona.jpg">'
},{
    "matricula":"2021030314",
    "nombre":"Peñaloza Pizarro Felipe Andres",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/peñaloza.png">'
},{
    "matricula":"2021030328",
    "nombre":"Garcia Gonzalez Jorge Enrique",
    "grupo":"TCI-7-3",
    "carrera":"Tecnologias de la Informacion",
    "foto":'<img src="/img/peraka.jpeg">'
}
];



for (i=0;i<alumno.length;i++){
    let tabla = document.getElementById('tabla-alumnos');
    let row = tabla.insertRow(); // Inserta una fila en la tabla
    // Inserta celdas en la fila
    let cell1 = row.insertCell(0); // Matricula
    let cell2 = row.insertCell(1); // Nombre
    let cell3 = row.insertCell(2); // Grupo
    let cell4 = row.insertCell(3); // Carrera
    let cell5 = row.insertCell(4); // Foto

    // Agrega los datos del alumno a las celdas
    cell1.innerHTML = alumno[i].matricula;
    cell2.innerHTML = alumno[i].nombre;
    cell3.innerHTML = alumno[i].grupo;
    cell4.innerHTML = alumno[i].carrera;
    cell5.innerHTML = alumno[i].foto;
}

let tabla = document.getElementById('tabla-alumnos');



