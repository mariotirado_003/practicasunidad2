/*declaracion de variables */

const btnCalcular = document.getElementById('btnCalcular')

btnCalcular.addEventListener("click", function () {
    //Obtener los datos de los input
    let valorAuto = document.getElementById('ValorAuto').value;
    let pInicial = document.getElementById('porcentaje').value;
    let plazos = document.getElementById('plazos').value;

    //Hacer los calculos
    let pagoInicial = valorAuto * (pInicial / 100);
    let totalFin = valorAuto - pagoInicial;
    let pagoMensual = totalFin / plazos;

    //Mostrar los datos
    document.getElementById('pagoInicial').value = pagoInicial;
    document.getElementById('totalfin').value = totalFin;
    document.getElementById('pagoMensual').value = pagoMensual;


})

function arribaMouse() {

    parrafo = document.getElementById('pa');
    parrafo.style.color = "#ff00ff";
    parrafo.style.fontSize = '25px';
    parrafo.style.textAlign = 'justify';


}

function salirMouse() {

    parrafo = document.getElementById('pa');
    parrafo.style.color = "red";
    parrafo.style.fontSize = '17px';
    parrafo.style.textAlign = 'left';

}

function limpiar() {

    let parrafo = document.getElementById("pa");
    parrafo.innerHTML = "";

}



/*Manejo de Arreglos*/

//declaracion de array con elementos enteros
let arreglo = [4, 89, 30, 10, 34, 89, 10, 5, 8, 28];

//Diseñar una funcion que recibe como argumento un arreglo de enteros e imprime cada elemento y el tamaño del arreglo

function mostrarArray(arreglo) {

    let tamaño = arreglo.length;
    for (let con = 0; con < arreglo.length; ++con) {

        console.log(con + ":" + arreglo[con]);

    }

    console.log("Tamaño :" + tamaño);

}

//function para demostrar el promedio de los elementos de array
function getAvg(arreglo) {

    let res = 0;
    for (let i = 0; i < arreglo.length; i++) {

        res += arreglo[i];

    }
    return (res / arreglo.length);

}


//funcion para mostrar los valores pares de un arreglo
const mostrarPares = (arreglo) => {
    let pares = [];
    for (let i = 0; i < arreglo.length; i++) {
        if (arreglo[i] % 2 == 0) pares.push(arreglo[i]);
    }
    return pares;
}
console.log(getAvg(arreglo));



//Funcion para mostrar el valor mayor de los elementos de un arreglo

function mostrarMayor(arreglo) {

    let tamaño = arreglo.length
    let mayor = 0;
    let posicion = 0;

    for (let con = 0; con < tamaño; con++) {
        if (arreglo[con > mayor]) {

            mayor = arreglo[con];
            posicion = con

        }
    }
    console.log("El numero mayor del arreglo es: " + mayor + "Y esta en la posicion" + posicion)
}

//Funcion para llenar con valores aleatorios el arreglo
function randomVar() {
    let tamaño = arreglo.length

}

//Funcion que muestra el valor menor y la posicion del arreglo


//Funcion para comprobar si el generador de numeros aleatorios es simetrico,
//es decir que la diferencia entre numeros pares o impares no sea mayor al 20%















////////////////////////////////////////////////////////////////////////////////////////////////////////////////
let alumno = [{

    "matricula": 2021030117,
    "nombre": "Lopez Acosta Jose Miguel",
    "grupo": "TI-73",
    "carrera": "Tecnologias de la Informacion",
    "foto": "/img/2021030117.jpg"

},
];

console.log("Matricula :" + alumno.matricula);
console.log("Nombre :" + alumno.nombre);

alumno.nombre = "Acosta Diaz Luis Marioo"

console.log("Nombre" + alumno.nombre);

//Objetos Compuestos

let cuentaBanco = {

    "numero": "241296",
    "banco": "BBVA BANCOMER",
    cliente: {
        "nombre": "Mario Tirado",
    "fechaNac": "2003-08-05",
    "sexo": "M",
    "saldo": "10200"},
}

console.log("Nombre :" + cuentaBanco.cliente.nombre);
console.log("Saldo :" + cuentaBanco.saldo);

cuentaBanco.cliente.sexo = "F";
console.log(cuentaBanco.cliente.sexo);

//Arreglo de productos

let productos = [{

    "codigo": "1001",
    "descripcion": "Atun",
    "precio": "34"
},

{
    "codigo": "1002",
    "descripcion": "Jabon en Polvo",
    "precio": "23"
},

{
    "codigo": "1003",
    "descripcion": "Harina",
    "precio": "43"
},

{
    "codigo": "1004",
    "descripcion": "Pasta Dental",
    "precio": "78"
}

]


//Mostrar el atributo del arreglo
console.log("La descripcion es " +productos[0].descripcion)

for(let i=0; i<productos.length; i++){
console.log("Codigo: "+productos[i].codigo);
console.log("Descripcion: "+productos[i].descripcion);
console.log("Precio: "+productos[i].precio);


}





