document.getElementById("btnRandom").addEventListener("click", function() {
    let cantidadNumeros = parseInt(document.getElementById("cantNum").value);
    let arregloAleatorio = llenarArregloAleatorio(cantidadNumeros);
    mostrarArray(arregloAleatorio);
    mostrarValoresPares(arregloAleatorio);
    let porcentajePares = calcularPorcentajePares(arregloAleatorio);
    let porcentajeImpares = 100 - porcentajePares;
    let esSimetrico = verificarSimetriaNumerosAleatorios(arregloAleatorio);

    let resultado = `Porcentaje de pares: ${porcentajePares.toFixed(2)}% \nPorcentaje de impares: ${porcentajeImpares.toFixed(2)}%\n `;
    resultado += `¿Es simétrico?: ${esSimetrico ? 'Sí' : 'No'}`;
    
    
    document.getElementById("results").innerText = resultado;
});

function llenarArregloAleatorio(longitud) {
    let arregloAleatorio = [];
    for (let con = 0; con < longitud; ++con) {
        arregloAleatorio.push(Math.floor(Math.random() * 100)); // genera números aleatorios entre 0 y 100
    }
    return arregloAleatorio;
}

function mostrarArray(arreglo) {
    let cmbNum = document.getElementById("cmbNum");
    cmbNum.innerHTML = ""; // Limpiar opciones previas
    for (let i = 0; i < arreglo.length; ++i) {
        let option = document.createElement("option");
        option.value = arreglo[i];
        option.text = arreglo[i];
        cmbNum.appendChild(option);
    }
}

function mostrarValoresPares(arreglo) {
    for (let con = 0; con < arreglo.length; ++con) {
        if (arreglo[con] % 2 === 0) {
            console.log(arreglo[con]);
        }
    }
}

function calcularPorcentajePares(arreglo) {
    let contadorPares = 0;
    for (let i = 0; i < arreglo.length; ++i) {
        if (arreglo[i] % 2 === 0) {
            contadorPares++;
        }
    }
    return (contadorPares / arreglo.length) * 100;
}

function verificarSimetriaNumerosAleatorios(arreglo) {
    let contadorPares = 0;
    let contadorImpares = 0;
    
    for (let i = 0; i < arreglo.length; i++) {
        if (arreglo[i] % 2 === 0) {
            contadorPares++;
        } else {
            contadorImpares++;
        }
    }
    
    let porcentajePares = (contadorPares / arreglo.length) * 100;
    let porcentajeImpares = (contadorImpares / arreglo.length) * 100;

    return Math.abs(porcentajePares - porcentajeImpares) <= 20;
}

// Función para mostrar números pares
function mostrarNumerosPares() {
    const numerosAleatorios = obtenerNumerosAleatorios();
    const numerosPares = numerosAleatorios.filter(numero => numero % 2 === 0);
    document.getElementById('numerosPares').textContent = `Números Pares: ${numerosPares.join(', ')}`;
}

// Función para mostrar el valor mayor y su posición
function mostrarValorMayorYPosicion() {
    const numerosAleatorios = obtenerNumerosAleatorios();
    const valorMayor = Math.max(...numerosAleatorios);
    const posicionValorMayor = numerosAleatorios.indexOf(valorMayor);
    document.getElementById('valorMayor').textContent = `Valor Mayor: ${valorMayor}, Posición: ${posicionValorMayor}`;
}

// Función para mostrar el promedio
function mostrarPromedio() {
    const numerosAleatorios = obtenerNumerosAleatorios();
    const promedio = calcularPromedio(numerosAleatorios);
    document.getElementById('promedio').textContent = `Promedio: ${promedio}`;
}

// Función para mostrar el valor menor y su posición
function mostrarValorMenorYPosicion() {
    const numerosAleatorios = obtenerNumerosAleatorios();
    const valorMenor = Math.min(...numerosAleatorios);
    const posicionValorMenor = numerosAleatorios.indexOf(valorMenor);
    document.getElementById('valorMenor').textContent = `Valor Menor: ${valorMenor}, Posición: ${posicionValorMenor}`;
}

// Función para obtener los números aleatorios generados
function obtenerNumerosAleatorios() {
    const cmbNum = document.getElementById('cmbNum');
    const numerosAleatorios = [];
    for (let i = 0; i < cmbNum.options.length; i++) {
        numerosAleatorios.push(parseInt(cmbNum.options[i].value));
    }
    return numerosAleatorios;
}

// Función para calcular el promedio de un arreglo
function calcularPromedio(arreglo) {
    const suma = arreglo.reduce((total, numero) => total + numero, 0);
    return suma / arreglo.length;
}

